'use strict';

$(function() {

  let phoneBook = JSON.parse(localStorage.getItem('phoneBook')) || [];
  let tableBody = $('#phone_book').find('tbody');

  function fillTable() {
    let html = '<tr>';
    phoneBook.forEach(function(value, index) {
      html +=
        `<tr>
            <td><input class="edit name" data-value="${value.name}" type="text" value="${value.name}"></td>
            <td><input class="edit phone" data-value="${value.phone}" type="text" value="${value.phone}"></td>
            <td>
                <button class="save" disabled data-index="${index}">Save</button>
                <button class="delete" data-index="${index}">Delete</button>
            </td>
        </tr>`;
    });

    tableBody.html(html);
  }

  function saveBook() {
    localStorage.setItem('phoneBook', JSON.stringify(phoneBook));
  }

  $('#add').on('click', function() {
    let name = $('#name');
    let phone = $('#phone');

    phoneBook.push({
      name: name.val(),
      phone: phone.val()
    });
    saveBook();
    name.val('');
    phone.val('');
    fillTable();
  });

  $(document).on('click', '.delete',function() {
    let index = $(this).data('index');
    phoneBook.splice(index, 1);
    saveBook();
    fillTable();
  });

  $(document).on('keyup', '.edit',function() {
    let saveButton = $(this).parents(':eq(1)').find('.save');
    saveButton.prop('disabled', ($(this).data('value').toString() === $(this).val().toString()));
  });

  $(document).on('click', '.save',function() {
    let index = $(this).data('index');
    let row = $(this).parents(':eq(1)');
    let name = row.find('.name').val();
    let phone = row.find('.phone').val();
    $(this).prop('disabled', true);
    phoneBook[index] = {
      name: name,
      phone: phone
    };
    saveBook();
  });

  fillTable();
});